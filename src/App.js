// base imports

import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";

// app imports

import UserContext from "./userContext";

// App Components

import AppNavbar from "./components/AppNavbar";

// page components

import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import NotFoundPage from "./notFound";

export default function App() {

    // localStorage.getItem - used to retrive a piece or the whole set of information inside the localStorage. the code below detects if there is a user logged in through the use of localStorage.SetItem in the Login.js

    
  const [user, setUser] = useState({ email: localStorage.getItem("email") });

  const unsetUser = () => {
      localStorage.clear()
      setUser({email:null})
  }

  /* 
        path="*" 0 all other unspecified rotues. thsi is to make sure that all other routes, beside the ones return statement will render the error page.

        The provider conmonent inside useContext is what allows other ocmponents to consume or use the contet. ANy omponent chich is not wrappedby the provide will have access to the values proided in the context
    */

  return (
    <UserContext.Provider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar user={user} />
        <notFound />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </Router>
    </UserContext.Provider>
  );
}
