import React, {useState, useEffect, useContext} from 'react'
import { Navigate } from 'react-router-dom'

// App Imports
import UserContext from '../userContext'


import {Form, Container, Button, Card} from 'react-bootstrap'
import Swal from 'sweetalert2'
import userContext from '../userContext'

export default function Login() {

    const {user, setUser} = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isDisabled, setIsDisabled] = useState('true')

    
    useEffect(() => {


        let isEmailNotEmpty = email !== null 
        let isPasswordnotEmpty = password !== null


        if (isEmailNotEmpty && isPasswordnotEmpty) {
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        }
    },[email, password])



    function Login(e){
        
        e.preventDefault()
        Swal.fire('You are now logged in')

        localStorage.setItem('email', email)

        // localStorage.setItem - to store a piece of data inside the localStorage of the browser, this is because the local



        setUser( {email: email} )
 
        setEmail('')
        setPassword('')
 
    }

    if (user.email !== null){
       //  return<Redirect to="/" /> - Redirect is a depricated function inside React and it is replaced by Navigate  

       /* 
       
        Navigate - allows us to redirect the users after logging in and updating the userstate. Even if the user tries to input the /login as URI it would stillredirect him/her to the home page.

                replace to (attribue) - to speify the page/uri to where the user/s will be redirected.
       
       */
        return<Navigate replace to="/courses" />
    }



    return(
        <Container className="login__page">

              
                
                <Form onSubmit={Login}>
                            <h1>
                                <div className="align-items-center justify-content-center">
                                Login
                                </div>
                            </h1>
                      
                    <Form.Group>
                        <Form.Label>
                            <h3>Email Address</h3>
                        </Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
                        
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>
                            <h3>Password</h3>
                        </Form.Label>
                        <Form.Control type="Password" placeholder="Enter Password" value={password} onChange={(e) => setPassword(e.target.value)} required />
                    </Form.Group>
                
                    <Button variant="primary" type="submit" disabled={isDisabled}>Login</Button>
                </Form>
        
        </Container>
    )
}
