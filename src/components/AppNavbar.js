import React, { Fragment, useContext } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";

// bootstrap components

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

// App imports
import UserContext from "../userContext";

// export default ~ allows the function to be used by the other files outside the file
export default function AppNavbar() {
  const { user, unsetUser } = useContext(UserContext);
  const history = useNavigate()

  const logout = () => {
      unsetUser()
      history('/login')

      // history.push("/login") - useHIstory is depicated in React. it was replace by use Navigate
  }

  let rightNav =
    user.email === null ? (
      <Fragment>
        <Nav.Link as={NavLink} to="/register">
          Register
        </Nav.Link>
        <Nav.Link as={NavLink} to="/login">
          Login
        </Nav.Link>
      </Fragment>
    ) : (
      <Fragment>
        <Nav.Link onClick={logout}>
          Logout
        </Nav.Link>

      </Fragment>
    );

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="/">
        Zuitt Booking
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={NavLink} to="/">
            Home
          </Nav.Link>
          <Nav.Link as={NavLink} to="/courses">
            Courses
          </Nav.Link>
        </Nav>
        <Nav className="ml-auto">{rightNav}</Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}   