import React from 'react';


/* class NotFoundPage extends React.Component{
    render(){
        return <div>
            <img src={PageNotFound}  />
            <p style={{textAlign:"center"}}>
              <Link to="/">Go to Home </Link>
            </p>
          </div>;
    }
}
export default NotFoundPage; */


import Jumbotron from 'react-bootstrap/Jumbotron'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row' 
import Col from 'react-bootstrap/Col' 

export default function NotFoundPage (){
    return(
        <Row>
            <Col>
                <Jumbotron>
                  
                    <h1>404 Not Found</h1>
                    <p>The Page you requested is not available</p>
                   
                </Jumbotron>
            </Col>
        </Row>
    )
}

